/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referring to this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-twitter': '&#xe600;',
		'icon-television': '&#xe601;',
		'icon-slideshare': '&#xe602;',
		'icon-pin-location': '&#xe603;',
		'icon-phone-2': '&#xe604;',
		'icon-phone-1': '&#xe605;',
		'icon-micro': '&#xe606;',
		'icon-messages': '&#xe607;',
		'icon-linkedin': '&#xe608;',
		'icon-graph': '&#xe609;',
		'icon-google-plus': '&#xe60a;',
		'icon-fax': '&#xe60b;',
		'icon-facebook': '&#xe60c;',
		'icon-camera': '&#xe60d;',
		'icon-diams': '&#xe60e;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
