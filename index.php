<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Hyperworld</title>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <!-- Mobile Specific Metas  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <!-- CSS  ================================================== -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/fonts.css">

    <!-- FONTS MODE DEV  ================================================== -->
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/6723452/632642/css/fonts.css" />


    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons ================================================== -->
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png"/>


        <!-- JS   ================================================== -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="./js/jquery.easing.1.3.js"></script>
    <script src="./js/jquery.scrolling-parallax.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.27/jquery.form-validator.min.js"></script>    
    <script src="http://malsup.github.io/min/jquery.form.min.js"></script>
    <script src="./js/jquery.tweet.js"></script>
    <script src="./js/main.js"></script>


    <script>

$(document).ready(function(){
	$('.paralax-1').parallax("50%", 0.5);
	$('.paralax-2').parallax("50%", 0.5);
	$('.paralax-3').parallax("50%", 0.5);
})
</script>

</head>
<body id="top">

	<header>
		<div class="container">
			<a href="#top" class="scroll" data-speed="1200" data-easing="easeInOutQuad" id="logo">
				<h1>
					Hyperworld
				</h1>
			</a>
			<div class="btn-nav">
				<div class="inner"></div>
			</div>
			<nav id="navigation">
				<ul>
					<li>
						<a class="scroll" data-speed="1200" data-easing="easeInOutQuad" href="#valeur">Notre offre</a>
					</li>
					<li>
						<a class="scroll" data-speed="1200" data-easing="easeInOutQuad" href="#references">references</a>
					</li>
					<li>
						<a class="scroll" data-speed="1200" data-easing="easeInOutQuad" href="#equipes">equipes</a>
					</li>
					<li>
						<a class="scroll" data-speed="1200" data-easing="easeInOutQuad" href="#contact">contact</a>
					</li>
					<div id="hover-bar"></div>
				</ul>
			</nav>
		</div>
	</header>

	<section class="big-image">
		<p>
			Nous faisons surgir l’information utile pour  optimiser en continu les contenus Medias et Marques 
		</p>
	</section>

	<section id="valeur" class="container first">
		<h2>
			Notre offre 
		</h2>
		<p class="editorial">
Avec plus de 60 000 interviews par an pour évaluer des contenus, Hyperworld Marketing a conquis rapidement <a href="#references" class="scroll" data-speed="1200" data-easing="easeInOutQuad">une clientèle de choix</a> dans le paysage médiatique grâce à  la pertinence de son offre 
		</p>
		<article class="eight columns icon">
			<div>
				<span class="icon-micro"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
			<div>
				<span class="icon-television"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
			<div>
				<span class="icon-camera"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
		</article>
		<article class="eight columns icon">
			<div>
				<span class="icon-diams"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
			<div>
				<span class="icon-phone-1"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
			<div>
				<span class="icon-graph"></span>
				<h3>
					Lorem ipsum dolor sit
				</h3>
				<p>
					Lorem ipsum dolor sit amet, onsectetur adipisicing elit, sed do eiusmod tempor 	incididunt ut labore. 
				</p>
			</div>
		</article>
		<article class="eight columns text">
			<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantiu.</p>
			<p>Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. <br>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur</p>
		</article>
		<article class="eight columns text">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim</p>
		</article>
	</section>

	<div class="plx paralax-1">
	</div>

	<section id="references" class="container second">

		<h2>
			references
		</h2>
		<p class="editorial">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
		</p>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/bureau_radio.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/radio-classique.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/nrj.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/lagardere.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/fip.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/rfi.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/europe1.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/le_mouv.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/france_inter.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/tsf_jazz.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/rfm.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/virgin.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/fg_radio.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/ado.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/latina.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/oui_fm.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/wit.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/vibration.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/voltage.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/kiss_fm.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/contact.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/radio_star.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/trace_fm.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/tropiques.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/radio_isa.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/mosaique.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>		
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/rtbf.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>
			</div>
		</div>

		<div class="three columns radio-group">
			<div class="radio-item">
				<div class="front face">
					<img src="./img/power_tork.jpg">
				</div>
				<div class="back face">
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit."
					<p class="signature">L.Abite</p>
					<img src="./img/virgin.png" class="logo">
				</div>			
			</div>
		</div>


	
	</section>

	<div class="plx paralax-2">
	</div>

	<section id="equipes" class="container third">
		<h2>
			équipes
		</h2>
		<article class="clearfix">
			<div class="four columns">
				<figure>
					<img src="img/roy.png">
					<figcaption>Henri-Paul <br> ROY</figcaption>
				</figure>
				<p>Directeur associé</p>
			</div>
			<div class="four columns">
				<figure>
					<img src="img/saint_roman.png">
					<figcaption>Arnaud de <br> SAINT-ROMAN</figcaption>
				</figure>
				<p>Directeur associé</p>
			</div>
			<div class="four columns">
				<figure>
					<img src="img/abdellaoui.png">
					<figcaption>Nora <br> ABDELLAOUI</figcaption>
				</figure>
				<p>Chef de projet</p>
			</div>
			<div class="four columns">
				<figure>
					<img src="img/le_houezec.png">
					<figcaption>Grégory <br> LE HOUËZEC</figcaption>
				</figure>
				<p>Chef de projet</p>
			</div>
		</article>
		<p class="editorial">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
		</p>

		<article class="description">
			<div class="six columns">

				<div class="row">
					<figure>
						<img src="img/roy-thumb.png">
						<figcaption>Henry-Paul ROY</figcaption>
					</figure>
					<p>Directeur Associé</p>
				</div>

				<div class="row"><span class="icon-twitter"></span><p>@hyperworld</p></div>
				<div class="row"><span class="icon-linkedin"></span><p>linked.in/hp.roy</p></div>
				<div class="row"><span class="icon-messages"></span><p>henripaul.roy@hyperwolrd.fr</p></div>
				<div class="row"><span class="icon-phone-2"></span><p>06 71 46 20 06</p></div>
			</div>
			<div class="ten columns">
				<div>
				<p>Ancien directeur des études ad hoc d’IPSOS MEDIAS, directeur des études éditoriales de NRJ GROUP, directeur du marketing de CANALWEB puis directeur du marketing de LAGARDERE ACTIVE FM, Henri-Paul dispose d’une large expérience du marketing des programmes radio, TV et Internet, et plus particulièrement des programmes musicaux.
				</p>
				<p>Passionné par les médias, les nouvelles technologies et la musique, Henri-Paul a introduit et développé en Europe les études de suivi de la programmation des réseaux musicaux, d’abord sous forme téléphonique (Call-Out) puis sur Internet (Call-Online). Il enseigne par ailleurs le marketing des médias à Sciences Po Paris.
				</p>
				</div>
			</div>
		</article>
		<article class="description">
			<div class="six columns">

				<div class="row">
					<figure>
						<img src="img/saint_roman-thumb.png">
						<figcaption>Arnaud de<br>SAINT-ROMAN</figcaption>
					</figure>
					<p>Directeur Associé</p>
				</div>

				<div class="row"><span class="icon-twitter"></span><p>@hyperworld</p></div>
				<div class="row"><span class="icon-linkedin"></span><p>linked.in/hp.roy</p></div>
				<div class="row"><span class="icon-messages"></span><p>henripaul.roy@hyperwolrd.fr</p></div>
				<div class="row"><span class="icon-phone-2"></span><p>06 71 46 20 06</p></div>
			</div>
			<div class="ten columns">
				<div>
				<p>Ancien directeur des études ad hoc d’IPSOS MEDIAS, directeur des études éditoriales de NRJ GROUP, directeur du marketing de CANALWEB puis directeur du marketing de LAGARDERE ACTIVE FM, Henri-Paul dispose d’une large expérience du marketing des programmes radio, TV et Internet, et plus particulièrement des programmes musicaux.
				</p>
				<p>Passionné par les médias, les nouvelles technologies et la musique, Henri-Paul a introduit et développé en Europe les études de suivi de la programmation des réseaux musicaux, d’abord sous forme téléphonique (Call-Out) puis sur Internet (Call-Online). Il enseigne par ailleurs le marketing des médias à Sciences Po Paris.
				</p>
				</div>
			</div>
		</article>
		<article class="description">
			<div class="six columns">

				<div class="row">
					<figure>
						<img src="img/abdellaoui-thumb.png">
						<figcaption>Nora ABDELLAOUI</figcaption>
					</figure>
					<p>Chef de projet</p>
				</div>

				<div class="row"><span class="icon-twitter"></span><p>@hyperworld</p></div>
				<div class="row"><span class="icon-linkedin"></span><p>linked.in/hp.roy</p></div>
				<div class="row"><span class="icon-messages"></span><p>henripaul.roy@hyperwolrd.fr</p></div>
				<div class="row"><span class="icon-phone-2"></span><p>06 71 46 20 06</p></div>
			</div>
			<div class="ten columns">
				<div>
				<p>Ancien directeur des études ad hoc d’IPSOS MEDIAS, directeur des études éditoriales de NRJ GROUP, directeur du marketing de CANALWEB puis directeur du marketing de LAGARDERE ACTIVE FM, Henri-Paul dispose d’une large expérience du marketing des programmes radio, TV et Internet, et plus particulièrement des programmes musicaux.
				</p>
				<p>Passionné par les médias, les nouvelles technologies et la musique, Henri-Paul a introduit et développé en Europe les études de suivi de la programmation des réseaux musicaux, d’abord sous forme téléphonique (Call-Out) puis sur Internet (Call-Online). Il enseigne par ailleurs le marketing des médias à Sciences Po Paris.
				</p>
				</div>
			</div>
		</article>
		<article class="description">
			<div class="six columns">

				<div class="row">
					<figure>
						<img src="img/le_houezec-thumb.png">
						<figcaption>Grégory<br>LE HOUËZEC</figcaption>
					</figure>
					<p>Chef de projet</p>
				</div>

				<div class="row"><span class="icon-twitter"></span><p>@hyperworld</p></div>
				<div class="row"><span class="icon-linkedin"></span><p>linked.in/hp.roy</p></div>
				<div class="row"><span class="icon-messages"></span><p>henripaul.roy@hyperwolrd.fr</p></div>
				<div class="row"><span class="icon-phone-2"></span><p>06 71 46 20 06</p></div>
			</div>
			<div class="ten columns">
				<div>
				<p>Ancien directeur des études ad hoc d’IPSOS MEDIAS, directeur des études éditoriales de NRJ GROUP, directeur du marketing de CANALWEB puis directeur du marketing de LAGARDERE ACTIVE FM, Henri-Paul dispose d’une large expérience du marketing des programmes radio, TV et Internet, et plus particulièrement des programmes musicaux.
				</p>
				<p>Passionné par les médias, les nouvelles technologies et la musique, Henri-Paul a introduit et développé en Europe les études de suivi de la programmation des réseaux musicaux, d’abord sous forme téléphonique (Call-Out) puis sur Internet (Call-Online). Il enseigne par ailleurs le marketing des médias à Sciences Po Paris.
				</p>
				</div>
			</div>
		</article>


	</section>
	<div class="plx paralax-3">
	</div>

	<section id="contact" class="container fourth">
		<h2>
			Contact
		</h2>

		<form action="mail.php" method="POST" class="eight columns" id="form">
            <input type="text" name="nom" placeholder="Nom" data-validation="required" data-validation-lenght="min2"/>
            <input type="text" name="prenom" placeholder="Prénom" data-validation="required" data-validation-lenght="min3"/>
            <input type="email" name="email" placeholder="Email" data-validation="email required"/>
            <input type="text" name="subject" placeholder="Sujet" data-validation="required"/>
            <textarea name="message" placeholder="Message" rows="5" data-validation="required" ></textarea>
            <input type="submit" value="envoyer" class="btn"/>
        </form>

        <div class="seven columns offset-by-one contact-info">

        	<div class="row"><span class="icon-pin-location"></span><p>4 Cité Paradis 75010 Paris<br>France</p></div>

        	<div class="row"><span class="icon-messages"></span><p>contact@hyperworld.fr</p></div>

        	<div class="row"><span class="icon-phone-2"></span><p>+33 9 50 80 42 95</p></div>

        	<div class="row"><span class="icon-fax"></span><p>+33 1 70 79 08 05</p></div>

        	<div class="row">
        	FOLLOW US ON :<br>
	        	<a target=_blank href="https://www.facebook.com/pages/Hyperworld-Marketing/193761757305884">
	        		<span class="icon-facebook"></span>
	        	</a>
	        	<a target=_blank href="https://twitter.com/RoyHP">
	        		<span class="icon-twitter"></span>
	        	</a>
	        	<a target=_blank href="#">
	        		<span class="icon-google-plus"></span>
	        	</a>
	        	<a target=_blank href="http://www.linkedin.com/vsearch/p?company=Hyperworld+Marketing&trk=prof-0-ovw-curr_pos">
	        		<span class="icon-linkedin"></span>
	        	</a>
	        	<a target=_blank href="http://fr.slideshare.net/HPRoy">
	        		<span class="icon-slideshare"></span>
        		</a>

        	</div>


        </div>


        <div class="sixteen columns">
        	<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x47e66e149568fdf9%3A0x62fbeb7dbb2c4673!2shyperworld!5e0!3m2!1sfr!2sfr!4v1388802877971" width="100%" height="400" frameborder="0" style="border:0"></iframe>
        </div>

	</section>

	<footer class="clearfix">
		<div class="container">
			<div class="columns-fter">
				<h5>Latest Tweets</h5>
				<div id="tweets"></div>
			</div>		
			<div class="columns-fter">
				<h5>Sitemap</h5>
				<p><a href="#top">Accueil</a></p>
				<p><a href="#valeurs">Notre offre</a></p>
				<p><a href="#references">Références</a></p>
				<p><a href="#equipes">Equipes</a></p>
				<p><a href="#contact">Contact</a></p>				
			</div>		
			<div class="columns-fter">
				<h5>Hyperworld websites</h5>
				<p><a href="#">Site</a></p>
				<p><a href="#">Site</a></p>
				<p><a href="#">Site</a></p>
				<p><a href="#">Site</a></p>
				<p><a href="#">Site</a></p>	
			</div>
		</div>

		<div class="bottom">
		Copyrights &copy; <?php echo date("Y"); ?> - All Rights Reserved by HyperWorld
		</div>
	</footer>

	<script src="./js/smooth-scroll.js"></script>

</body>
</html>