$(document).ready(function() {

/* SLIDER et positionnement du texte */

$windowH = $(window).height();
$sliderP = $('.big-image p').height();
$('.big-image').css ({
	"height" : $windowH
});
$('.big-image p').css ({
	"padding-top" : ($windowH/2)-($sliderP/2)
});

});



$(window).load(function() {
	
/*MENU RESPONSIVE*/

if (window.matchMedia("(max-width: 640px)").matches) {
	$(function(){
		$("#banner").addClass('slideUp');
		$(".btn-nav").click(function(e){
			e.preventDefault();
			$(this).toggleClass("open");
			$('[id="navigation"]').slideToggle("600");
		});
		$('#navigation li').click(function(){
			$('#navigation').slideToggle("600");
			$(".btn-nav").removeClass("open");
		});
	})
}


/* NAVIGATION */

var sections = $('section');
var menuItems = $('nav ul li');
$itemActif = 0;


$(menuItems).click(function() {
	$(menuItems).removeClass('active')
	$(this).addClass("active");
})

$(window).scroll(function() {

    var currentPosition = ($(this).scrollTop()+200);

    sections.removeClass('active').each(function() {
        var top = $(this).offset().top,
            bottom = top + $(this).height();

        if (currentPosition >= top && currentPosition <= bottom) {
            menuItems.removeClass('active');
            menuItems.eq(($(this).index())/2).addClass('active');
            $itemActif = $(this).index()/2;
        }
    });

    if ($itemActif == 0 ) {
		$('#hover-bar').css({
			"background-color" :"none",
			"margin-left" : "0"
		})
	}
    if ($itemActif == 1 ) {
		$('#hover-bar').css({
			"background-color" :" #9c3f7f",
			"margin-left" : (($itemActif-1)*159)
		})
	}
	if ($itemActif == 2 ) {
		$('#hover-bar').css({
			"background-color" :" #ff9900",
			"margin-left" : (($itemActif-1)*159)
		})
	}
	if ($itemActif == 3 ) {
		$('#hover-bar').css({
			"background-color" :" #ff4833",
			"margin-left" : (($itemActif-1)*165)
		})
	}
	if ($itemActif == 4 ) {
		$('#hover-bar').css({
			"background-color" :" #ffd03d",
			"margin-left" : (($itemActif-1)*159)
		})
	}
});


/* Animations EQUIPES */  


$carte = $('.third .four');
$personnageFiche = $('.third .description');

$carte.eq(0).css({
	"opacity" : "1",
    "filter" : "grayscale(0%)",
	"-webkit-filter" : "grayscale(0%)",
	"-moz-filter" : "grayscale(0%)",
	"-ms-filter" : "grayscale(0%)",
	"-o-filter" : "grayscale(0%)"
});

$personnageFiche.eq(0).show();


$carte.click(function(){
	$personnageFiche.hide();
	$personnageActif = $personnageFiche.eq($(this).index())
	$personnageActif.fadeIn();

	$carte.css({
		"opacity":"0.7",
        "filter" : "grayscale(100%)",
        "-webkit-filter" : "grayscale(100%)",
        "-moz-filter" : "grayscale(100%)",
        "-ms-filter" : "grayscale(100%)",
        "-o-filter" : "grayscale(100%)"
	})

	$(this).css({
	"opacity" : "1",
    "filter" : "grayscale(0%)",
	"-webkit-filter" : "grayscale(0%)",
	"-moz-filter" : "grayscale(0%)",
	"-ms-filter" : "grayscale(0%)",
	"-o-filter" : "grayscale(0%)"
	});


})

/* FORM VALIDATOR + AJAX */

$formValid = false;
$.validate({
	form : '#form',
	errorMessagePosition : 'top',
	onError : function() {
		event.preventDefault();
   		$formValid = false;
	},

	onSuccess : function() {
		$('#form .btn').val('Merci');
		$('#form .btn').css({"cursor" : "default","opacity" : "0.7"});
		/*$('#form').ajaxForm();*/
		event.preventDefault();
		$formValid = true;
	}
});	
$('#form .btn').click(function(){
	if ($formValid = false) {return false;}
	else {$('#form').ajaxForm();}
});



/*  TWITTER UPDATE  */





});




